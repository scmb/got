package it.netgrid.got.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class PropertiesConfigurationTemplate {
	private Properties properties;
	
	private void loadProperties(String propertiesFilePathOrResourceName) {
		if(propertiesFilePathOrResourceName != null) {
			// Try to load argument as File Properties
			if(loadPropertiesFromPath(propertiesFilePathOrResourceName)) return;
			// Try to load argument as Resource Properties
			if(loadPropertiesAsResource(propertiesFilePathOrResourceName)) return;
		}
		// Try to load properties from Default Path
		if(loadPropertiesFromPath(getDefaultConfigPropertiesPath())) return;
		loadPropertiesAsResource(getDefaultConfigPropertiesResource());
	}
	private boolean loadPropertiesAsResource(String propertiesResourceName) {
		if (properties == null) {
			try (InputStream resourceStream = getClass().getClassLoader()
					.getResourceAsStream(propertiesResourceName);) {
				properties = new Properties();
				properties.load(resourceStream);
			} catch (NullPointerException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return properties != null;
	}

	private boolean loadPropertiesFromPath(String propertiesFilePath) {
		if(properties==null){
		FileInputStream in = null;
		try {
			in = new FileInputStream(propertiesFilePath);
		} catch (FileNotFoundException e) {
			return false;
		}
		try {
			properties = new Properties();
			properties.load(in);
		} catch (IOException e) {
			return false;
		}
		try {
			in.close();
		} catch (IOException e) {
			return false;
		}
		}
		return properties != null;
	}
	
	public Properties getProperties(String propertiesFilePathOrResourceName){
		loadProperties(propertiesFilePathOrResourceName);
		return properties;
	}
	
	
	public abstract String getDefaultConfigPropertiesPath();
	public abstract String getDefaultConfigPropertiesResource();
	
	
		
	}

