package it.netgrid.got.model;

public class ComunicationUtils {
	public class Gatekeeper{
		public static final String SEPARATOR="#";
		public static final String UNKNOWN = "404";
		public static final String MAYBE = SEPARATOR + "maybe";
		public static final String SURE = SEPARATOR + "sure";
	}
}
