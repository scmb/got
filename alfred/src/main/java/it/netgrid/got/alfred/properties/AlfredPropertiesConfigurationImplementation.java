package it.netgrid.got.alfred.properties;

import java.util.Properties;

import it.netgrid.got.utils.PropertiesConfigurationTemplate;

public class AlfredPropertiesConfigurationImplementation extends PropertiesConfigurationTemplate
		implements AlfredPropertiesConfiguration {
	private static final String DEFAULT_CONFIG_PROPERTIES_PATH = System.getProperty("user.dir")+"/alfred.properties";
	private static final String DEFAULT_CONFIG_PROPERTIES_RESOURCE = "alfred.properties";
	private Properties properties;

	public AlfredPropertiesConfigurationImplementation() {
		properties = getProperties(null);
	}

	@Override
	public String getDefaultConfigPropertiesPath() {
		return DEFAULT_CONFIG_PROPERTIES_PATH;
	}

	@Override
	public String getDefaultConfigPropertiesResource() {
		return DEFAULT_CONFIG_PROPERTIES_RESOURCE;
	}

	@Override
	public String getTelegramSendTopic() {
		return properties.getProperty("telegram_send");
	}

	@Override
	public String getTelegramReceiveTopic() {
		return properties.getProperty("telegram_receive");
	}

	@Override
	public String getTelegramErrorTopic() {
		return properties.getProperty("telegram_error");
	}

	@Override
	public String getGatekeeperSendTopic() {
		return properties.getProperty("gatekeeper_send");
	}

	@Override
	public String getGatekeeperReceiveTopic() {
		return properties.getProperty("gatekeeper_receive");
	}

	@Override
	public String getGatekeeperErrorTopic() {
		return properties.getProperty("gatekeeper_error");
	}

}
