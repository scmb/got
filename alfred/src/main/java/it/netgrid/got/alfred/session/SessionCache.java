package it.netgrid.got.alfred.session;

import com.google.inject.Singleton;

@Singleton
public class SessionCache {
	private Long id;

	public SessionCache() {
	}

	public void register(Long id) {
		this.id = id;
	}

	public boolean contains(Long key) {
		return id == key;
	}

	public Long getUser() {
		return id;
	}
}
