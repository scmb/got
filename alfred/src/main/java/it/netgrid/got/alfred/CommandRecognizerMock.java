package it.netgrid.got.alfred;

import java.util.Date;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import it.netgrid.got.alfred.events.MessageToTelegram;
import it.netgrid.got.alfred.events.MessageFromTelegram;
import it.netgrid.got.alfred.session.SessionCache;

@Singleton
public class CommandRecognizerMock implements CommandRecognizer {
	public SessionCache cache;

	@Inject
	public CommandRecognizerMock(SessionCache cache) {
		this.cache = cache;
	}

	@Override
	public MessageToTelegram executeOnTelegram(MessageFromTelegram message, boolean confirming) {
		long chatId = message.getChat_id();
		if (message.getContent().equals("/start")) {
			if (!cache.contains(message.getChat_id())) {
				cache.register(message.getChat_id());
				return standardMessage(chatId, Messages.WELLCOME_MESSAGE);
			} else {
				return standardMessage(chatId, Messages.WELLCOME_BACK);
			}
		} else if (this.isOpen(message.getContent())) {
			if (confirming){
				return openGateMessage(message.getChat_id(), Messages.CONFIRMATION);
			}
			if(!confirming&&isImperativeOpen(message.getContent())){
				return openGateMessage(message.getChat_id(), Messages.CONFIRMATION);
			}
			else{
				return standardMessage(chatId, Messages.Door.NO_ONE_AT_DOORS);
			}
		} else if (this.isIgnore(message.getContent())) {
			if (confirming)
				return standardMessage(chatId, Messages.CONFIRMATION);
			else
				return standardMessage(chatId, Messages.Door.NO_ONE_AT_DOORS);
		} else {
			return standardMessage(chatId, Messages.UNKNOWN);
		}
	}

	private boolean isImperativeOpen(String content) {
		String message=content.toLowerCase();
		if(message.contains("aprimi")){
			return true;
		}
		if(message.contains("comunque")||message.contains("lo stesso")){
			return true;
		}
		return false;
	}

	private MessageToTelegram standardMessage(long chatId, String message) {
		return new MessageToTelegram(chatId, "alfred", message, new Date().getTime());
	}

	private MessageToTelegram openGateMessage(long chatId, String message) {
		MessageToTelegram event = new MessageToTelegram(chatId, "alfred", message, new Date().getTime());
		event.addCommand("gate", "open");
		return event;
	}

	@Override
	public SessionCache getCachedUsers() {
		return this.cache;
	}

	public boolean isOpen(String message) {
		String messageLow = message.toLowerCase();
			if ((messageLow.contains("apri")||messageLow.contains("entrare")) && !messageLow.contains("non")){
				return true;
			} else {
				return false;
			}
	}

	public boolean isIgnore(String message) {
		String messageLow = message.toLowerCase();
		if (messageLow.contains("ignora")&&!messageLow.contains("non")) {
			return true;
		}
		else if ((messageLow.contains("apri")||messageLow.contains("entrare")) && messageLow.contains("non")) {
			return true;
		}
		return false;
	}
}
