package it.netgrid.got.alfred.injector;

import com.google.inject.AbstractModule;

public class AlfredEnv extends AbstractModule {

	@Override
	protected void configure() {
		install(new TestModule());
	}

}
