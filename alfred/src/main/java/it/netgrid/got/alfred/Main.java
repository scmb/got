package it.netgrid.got.alfred;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.inject.Guice;
import com.google.inject.Injector;

import it.netgrid.got.alfred.injector.ConfigurationModule;

public class Main {
	static final int POOL_SIZE=1;

	public static void main(String[] args) {
		ExecutorService executor=Executors.newFixedThreadPool(POOL_SIZE);
		Injector injector = Guice.createInjector(new ConfigurationModule());
		Alfred alfred = injector.getInstance(Alfred.class);
		executor.execute(alfred);
	}

}
