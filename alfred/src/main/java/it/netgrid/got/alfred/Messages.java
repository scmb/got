package it.netgrid.got.alfred;

public class Messages {
	public static final String WELLCOME_MESSAGE = "Benvenuto, io sono Alfred.\n"
			+ "Sono un bot che fa da maggiordomo a questo stabile grazie all'Intelligenza Artificiale, i servizi Cloud e l'Internet Of Things!\n"
			+ "Al momento sono solo un proof of concept quindi le mie capacità si limitano a mandarle un messaggio quando qualcuno suona al campanello "
			+ "e ad aprire la porta se lei lo desidera, ma le mie potenzialità sono pressoché infinite.";
	public static final String WELLCOME_BACK = "Bentornato, signor Wayne.";
	public static final String CONFIRMATION = "Come desidera, signor Wayne.";
	public static final String UNKNOWN = "Quello che mi sta chiedendo è fuori dalle mie conoscenze al momento.";
	public class Door{
		public static final String SOMEBODY_AT_THE_DOOR="C'è qualcuno alla porta, signor Wayne.";
		public static final String A_SPECIFIED_PERSON_AT_THE_DOOR="%s è alla porta, signor Wayne.";
		public static final String MAYBE_A_SPECIFIED_PERSON_AT_THE_DOOR="Forse %s è alla porta, signor Wayne.";
		public static final String NO_ONE_AT_DOORS = "Non ha suonato nessuno, signor Wayne.";
			
	}
}
