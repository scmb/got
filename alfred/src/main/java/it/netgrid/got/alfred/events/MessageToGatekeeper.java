package it.netgrid.got.alfred.events;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MessageToGatekeeper {
	private String sender;
	private String content;

	public MessageToGatekeeper(String sender, String content) {
		this.setSender(sender);
		this.setContent(content);
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}