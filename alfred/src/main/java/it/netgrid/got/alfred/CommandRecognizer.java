package it.netgrid.got.alfred;

import it.netgrid.got.alfred.events.MessageToTelegram;
import it.netgrid.got.alfred.events.MessageFromTelegram;
import it.netgrid.got.alfred.session.SessionCache;

public interface CommandRecognizer {
	/**
	 * E' stato messo il parametro confirming per dire che alfred si aspetta un
	 * messaggio di conferma dell'apertura o meno, La cosa corretta sarebbe
	 * implementare una macchina a stati ma sarebbe un giorno in più di lavoro
	 * quindi al momento provo a tenere questa cosa
	 * 
	 * @param message
	 * @param confirming
	 * @return
	 */
	public MessageToTelegram executeOnTelegram(MessageFromTelegram message, boolean confirming);

	public SessionCache getCachedUsers();
}