package it.netgrid.got.alfred.properties;

public interface AlfredPropertiesConfiguration {

	public String getTelegramSendTopic();

	public String getTelegramReceiveTopic();

	public String getTelegramErrorTopic();

	public String getGatekeeperSendTopic();

	public String getGatekeeperReceiveTopic();

	public String getGatekeeperErrorTopic();
}
