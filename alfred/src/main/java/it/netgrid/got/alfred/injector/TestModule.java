package it.netgrid.got.alfred.injector;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import it.netgrid.got.alfred.CommandRecognizerMock;
import it.netgrid.got.alfred.session.SessionCache;

public class TestModule extends AbstractModule {

	@Override
	protected void configure() {
		install(new ConfigurationModule());
	}

	@Provides
	@Singleton
	public CommandRecognizerMock buildRecognizerMock(SessionCache cache) {
		return new CommandRecognizerMock(cache);
	}

}
