package it.netgrid.got.alfred;

import java.util.Date;

import com.google.inject.Inject;

import it.netgrid.bauer.EventHandler;
import it.netgrid.bauer.Topic;
import it.netgrid.got.alfred.events.GatekeeperError;
import it.netgrid.got.alfred.events.MessageToGatekeeper;
import it.netgrid.got.alfred.events.MessageFromGatekeeper;
import it.netgrid.got.alfred.events.TelegramError;
import it.netgrid.got.alfred.events.MessageToTelegram;
import it.netgrid.got.alfred.events.MessageFromTelegram;
import it.netgrid.got.alfred.properties.AlfredPropertiesConfiguration;
import it.netgrid.got.model.ComunicationUtils;

public class Alfred implements Runnable {

	private Topic<MessageFromTelegram> telegramSendTopic;
	private Topic<MessageToTelegram> telegramReceiveTopic;
	private Topic<TelegramError> telegramErrorTopic;
	private Topic<MessageFromGatekeeper> gatekeeperSendTopic;
	private Topic<MessageToGatekeeper> gatekeeperReceiveTopic;
	private Topic<GatekeeperError> gatekeeperErrorTopic;
	private CommandRecognizer recognizer;
	private AlfredPropertiesConfiguration properties;
	private boolean confirming;

	@Inject
	public Alfred(Topic<MessageFromTelegram> telegramSendTopic, Topic<MessageToTelegram> telegramReceiveTopic,
			Topic<TelegramError> telegramErrorTopic, Topic<MessageFromGatekeeper> gatekeeperSendTopic,
			Topic<MessageToGatekeeper> gatekeeperReceiveTopic, Topic<GatekeeperError> gatekeeperErrorTopic,
			CommandRecognizer recognizer, AlfredPropertiesConfiguration properties) {
		this.telegramSendTopic = telegramSendTopic;
		this.telegramReceiveTopic = telegramReceiveTopic;
		this.telegramErrorTopic = telegramErrorTopic;
		this.gatekeeperSendTopic = gatekeeperSendTopic;
		this.gatekeeperReceiveTopic = gatekeeperReceiveTopic;
		this.gatekeeperErrorTopic = gatekeeperErrorTopic;
		this.recognizer = recognizer;
		this.properties = properties;
	}

	@Override
	public void run() {
		listenOnGatekeeperError();
		listenOnGatekeeper();
		listenOnTelegramError();
		listenOnTelegram();
	}

	public void listenOnGatekeeper() {
		gatekeeperSendTopic.addHandler(new EventHandler<MessageFromGatekeeper>() {

			@Override
			public Class<MessageFromGatekeeper> getEventClass() {
				return MessageFromGatekeeper.class;
			}

			@Override
			public String getName() {
				return properties.getGatekeeperSendTopic();
			}

			@Override
			public boolean handle(MessageFromGatekeeper event) {
				try {
					telegramReceiveTopic.post(buttonPressionMessage(elaborateRecogResult(event.getContent())));
				} catch (Exception e) {
					e.printStackTrace();
				}
				return true;
			}
		});
	}

	public void listenOnTelegram() {

		telegramSendTopic.addHandler(new EventHandler<MessageFromTelegram>() {

			@Override
			public Class<MessageFromTelegram> getEventClass() {
				return MessageFromTelegram.class;
			}

			@Override
			public String getName() {
				return properties.getTelegramSendTopic();
			}

			@Override
			public boolean handle(MessageFromTelegram event) {
				MessageToTelegram response = recognizer.executeOnTelegram(event, confirming);
				confirming = false;	
				if (response.getCommands().containsKey("gate")) {
					gatekeeperReceiveTopic.post(new MessageToGatekeeper("alfred", "open"));
				}
				telegramReceiveTopic.post(response);
				return true;
			}
		});
	}

	public void listenOnGatekeeperError() {
		gatekeeperErrorTopic.addHandler(new EventHandler<GatekeeperError>() {

			@Override
			public Class<GatekeeperError> getEventClass() {
				return GatekeeperError.class;
			}

			@Override
			public String getName() {
				return properties.getGatekeeperErrorTopic();
			}

			@Override
			public boolean handle(GatekeeperError event) {
				// TODO errore da gatekeeper
				return true;
			}
		});
	}

	public void listenOnTelegramError() {
		telegramErrorTopic.addHandler(new EventHandler<TelegramError>() {

			@Override
			public Class<TelegramError> getEventClass() {
				return TelegramError.class;
			}

			@Override
			public String getName() {
				return properties.getTelegramErrorTopic();
			}

			@Override
			public boolean handle(TelegramError event) {
				// TODO errore da telegram
				return true;
			}
		});
	}

	private MessageToTelegram buttonPressionMessage(String message) {
		confirming = true;
		return new MessageToTelegram(recognizer.getCachedUsers().getUser(), "alfred", message, new Date().getTime());
	}
	
	private String elaborateRecogResult(String result){
		if(result.endsWith(ComunicationUtils.Gatekeeper.MAYBE)){
			return Messages.Door.MAYBE_A_SPECIFIED_PERSON_AT_THE_DOOR.replace("%s", result.split(ComunicationUtils.Gatekeeper.SEPARATOR)[0]);
		}
		else if(result.endsWith(ComunicationUtils.Gatekeeper.SURE)){
			return Messages.Door.A_SPECIFIED_PERSON_AT_THE_DOOR.replace("%s", result.split(ComunicationUtils.Gatekeeper.SEPARATOR)[0]);
		}
		else{
			return Messages.Door.SOMEBODY_AT_THE_DOOR;
		}
	}
}