package it.netgrid.got.alfred.events;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MessageToTelegram {
	private String sender;
	private String content;
	private Long timestamp;
	private Long chat_id;
	private Map<String, String> commands;

	public MessageToTelegram() {

	}

	public MessageToTelegram(Long chat_id, String sender, String content, Long timestamp) {
		this.setChat_id(chat_id);
		this.setSender(sender);
		this.setContent(content);
		this.setTimestamp(timestamp);
		this.commands = new HashMap<String, String>();
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Long getChat_id() {
		return chat_id;
	}

	public void setChat_id(Long chat_id) {
		this.chat_id = chat_id;
	}

	public void addCommand(String key, String value) {
		commands.put(key, value);
	}

	public Map<String, String> getCommands() {
		return commands;
	}

}
