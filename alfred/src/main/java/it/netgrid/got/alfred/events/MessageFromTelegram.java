package it.netgrid.got.alfred.events;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MessageFromTelegram {
	private String sender;
	private String content;
	private Long timestamp;
	private Long chat_id;

	public MessageFromTelegram() {

	}

	public MessageFromTelegram(Long chat_id, String sender, String content, Long timestamp) {
		this.setChat_id(chat_id);
		this.setSender(sender);
		this.setContent(content);
		this.setTimestamp(timestamp);
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Long getChat_id() {
		return chat_id;
	}

	public void setChat_id(Long chat_id) {
		this.chat_id = chat_id;
	}

}
