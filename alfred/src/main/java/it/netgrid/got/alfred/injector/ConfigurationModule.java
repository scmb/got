package it.netgrid.got.alfred.injector;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import it.netgrid.bauer.Topic;
import it.netgrid.bauer.TopicFactory;
import it.netgrid.got.alfred.Alfred;
import it.netgrid.got.alfred.CommandRecognizer;
import it.netgrid.got.alfred.CommandRecognizerMock;
import it.netgrid.got.alfred.events.GatekeeperError;
import it.netgrid.got.alfred.events.MessageToGatekeeper;
import it.netgrid.got.alfred.events.MessageFromGatekeeper;
import it.netgrid.got.alfred.events.TelegramError;
import it.netgrid.got.alfred.events.MessageToTelegram;
import it.netgrid.got.alfred.events.MessageFromTelegram;
import it.netgrid.got.alfred.properties.AlfredPropertiesConfiguration;
import it.netgrid.got.alfred.properties.AlfredPropertiesConfigurationImplementation;
import it.netgrid.got.alfred.session.SessionCache;

public class ConfigurationModule extends AbstractModule {
	@Override
	protected void configure() {
	}

	@Provides
	public Alfred buildAlfred(Topic<MessageFromTelegram> sendTopic, Topic<MessageToTelegram> receiveTopic,
			Topic<TelegramError> errorTopic, Topic<MessageFromGatekeeper> gatekeeperSendTopic,
			Topic<MessageToGatekeeper> gatekeeperReceiveTopic, Topic<GatekeeperError> gatekeeperErrorTopic,
			CommandRecognizer recognizer, AlfredPropertiesConfiguration conf) {
		return new Alfred(sendTopic, receiveTopic, errorTopic, gatekeeperSendTopic, gatekeeperReceiveTopic,
				gatekeeperErrorTopic, recognizer, conf);

	}

	@Provides
	@Singleton
	public Topic<MessageFromTelegram> buildTelegramSendTopic(AlfredPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getTelegramSendTopic());
	}

	@Provides
	@Singleton
	public Topic<MessageToTelegram> buildTelegramReceiveTopic(AlfredPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getTelegramReceiveTopic());
	}

	@Provides
	@Singleton
	public Topic<TelegramError> buildTelegramErrorTopic(AlfredPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getTelegramErrorTopic());
	}

	@Provides
	@Singleton
	public Topic<MessageFromGatekeeper> buildGatekeeperSendTopic(AlfredPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getGatekeeperSendTopic());
	}

	@Provides
	@Singleton
	public Topic<MessageToGatekeeper> buildGatekeeperReceiveTopic(AlfredPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getGatekeeperReceiveTopic());
	}

	@Provides
	@Singleton
	public Topic<GatekeeperError> buildGatekeeperErrorTopic(AlfredPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getGatekeeperErrorTopic());
	}

	@Provides
	@Singleton
	public CommandRecognizer buildRecognizer(SessionCache cache) {
		return new CommandRecognizerMock(cache);
	}

	@Provides
	@Singleton
	public SessionCache buildCache() {
		return new SessionCache();
	}

	@Provides
	@Singleton
	public AlfredPropertiesConfiguration buildConfiguration() {
		return new AlfredPropertiesConfigurationImplementation();
	}
}
