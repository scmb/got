package it.netgrid.got.alfred.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;

import com.google.guiceberry.junit4.GuiceBerryRule;
import com.google.inject.Inject;

import it.netgrid.got.alfred.CommandRecognizerMock;

public class MessageTesting {

	@Rule
	public final GuiceBerryRule guiceBerry = new GuiceBerryRule(AlfredTestEnv.class);

	@Inject
	CommandRecognizerMock command;
	List<String> opens;
	List<String> ignores;

	private List<String> generateOpen() {
		List<String> opens = new ArrayList<String>();
		opens.add("apri");
		opens.add("fallo entrare");
		opens.add("apri la porta");
		opens.add("apri pure");
		return opens;
	}

	private List<String> generateIgnore() {
		List<String> ignores = new ArrayList<String>();
		ignores.add("non aprire");
		ignores.add("non far entrare quel pirla");
		ignores.add("ignoralo");
		return ignores;
	}

	
	public void testCommand() {
		opens = this.generateOpen();
		ignores = this.generateIgnore();
		for (String message : opens) {
			assertTrue(command.isOpen(message));
			assertFalse(command.isIgnore(message));
		}
		for (String message : ignores) {
			assertTrue(command.isIgnore(message));
			assertFalse(command.isOpen(message));
		}
	}
	
}
