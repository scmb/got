package it.netgrid.got.alfred.test;

import com.google.guiceberry.GuiceBerryModule;

import it.netgrid.got.alfred.injector.AlfredEnv;

public class AlfredTestEnv extends AlfredEnv {

	@Override
	protected void configure() {
		super.configure();
		install(new GuiceBerryModule());
	}

}
