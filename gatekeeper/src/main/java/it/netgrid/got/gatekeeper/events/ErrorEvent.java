package it.netgrid.got.gatekeeper.events;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ErrorEvent {
	private String sender;
	private String content;

	public ErrorEvent(String sender, String content) {
		this.setSender(sender);
		this.setContent(content);
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
