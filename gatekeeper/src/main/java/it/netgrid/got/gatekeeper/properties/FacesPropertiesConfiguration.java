package it.netgrid.got.gatekeeper.properties;

import java.util.Properties;

public interface FacesPropertiesConfiguration {

	public String getName(String url);
	public String getUrl(String name);
	public Properties getProperties();
}
