package it.netgrid.got.gatekeeper.faces;

public class FaceMatchResult {
	private double match;
	private String name;

	public FaceMatchResult(double match, String name) {
		setMatch(match);
		setName(name);
	}

	public double getMatch() {
		return match;
	}

	private void setMatch(double match) {
		this.match = match;
	}

	public String getName() {
		return name;
	}

	private void setName(String name) {
		this.name = name.split("_")[0].replace(".", " ");
	}
}
