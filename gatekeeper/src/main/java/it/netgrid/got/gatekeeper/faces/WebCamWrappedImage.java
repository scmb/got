package it.netgrid.got.gatekeeper.faces;

import java.util.Date;

import org.openimaj.image.MBFImage;

public class WebCamWrappedImage {
	private MBFImage face;
	private Date timestamp;

	public WebCamWrappedImage(MBFImage face) {
		setFace(face);
	}

	public MBFImage getFace() {
		return face;
	}

	private void setFace(MBFImage face) {
		this.face = face;
		setTimestamp();
	}

	public Date getTimestamp() {
		return timestamp;
	}

	private void setTimestamp() {
		this.timestamp = new Date();
	}

}
