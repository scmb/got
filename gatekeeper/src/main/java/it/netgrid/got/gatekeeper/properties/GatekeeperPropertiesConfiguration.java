package it.netgrid.got.gatekeeper.properties;



public interface GatekeeperPropertiesConfiguration {
	public String getSendTopic();
	public String getReceiveTopic();
	public String getErrorTopic();
	public String getGateTopic();
	public String getButtonTopic();
	public String getCloudErrorTopic();
	public String getVideoStreamAddress();
	public String getFrameDir();
	public String getFacesDir();
	public int getQueueSize();
	public int getFrameRate();
}
