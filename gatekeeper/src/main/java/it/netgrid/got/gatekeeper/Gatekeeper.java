package it.netgrid.got.gatekeeper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.inject.Inject;

import it.netgrid.bauer.EventHandler;
import it.netgrid.bauer.Topic;
import it.netgrid.got.gatekeeper.events.EdcErrorEvent;
import it.netgrid.got.gatekeeper.events.EdcReceiveEvent;
import it.netgrid.got.gatekeeper.events.EdcSendEvent;
import it.netgrid.got.gatekeeper.events.ErrorEvent;
import it.netgrid.got.gatekeeper.events.ReceiveEvent;
import it.netgrid.got.gatekeeper.events.SendEvent;
import it.netgrid.got.gatekeeper.faces.Recognizer;


public class Gatekeeper implements Runnable {
	private Topic<SendEvent> sendTopic;
	private Topic<ReceiveEvent> receiveTopic;
	private Topic<ErrorEvent> errorTopic;
	private Topic<EdcSendEvent> gateTopic;
	private Topic<EdcReceiveEvent> buttonTopic;
	private Topic<EdcErrorEvent> cloudErrorTopic;
	private Recognizer recog;
	private ExecutorService executor = Executors.newFixedThreadPool(1);

	@Inject
	public Gatekeeper(Topic<SendEvent> sendTopic, Topic<ReceiveEvent> receiveTopic, Topic<ErrorEvent> errorTopic,
			Topic<EdcSendEvent> gateTopic, Topic<EdcReceiveEvent> buttonTopic, Topic<EdcErrorEvent> cloudErrorTopic,
			Recognizer recog) {
		this.sendTopic = sendTopic;
		this.receiveTopic = receiveTopic;
		this.errorTopic = errorTopic;
		this.gateTopic = gateTopic;
		this.buttonTopic = buttonTopic;
		this.cloudErrorTopic = cloudErrorTopic;
		this.recog = recog;
	}

	@Override
	public void run() {
		executor.execute(recog);

		receiveTopic.addHandler(new EventHandler<ReceiveEvent>() {

			@Override
			public Class<ReceiveEvent> getEventClass() {
				return ReceiveEvent.class;
			}

			@Override
			public String getName() {
				return receiveTopic.getName();
			}

			@Override
			public boolean handle(ReceiveEvent event) {
				gateTopic.post(new EdcSendEvent("gatekeeper", EdcMapBuilder.gatePayload()));
				return true;
			}
		});

		buttonTopic.addHandler(new EventHandler<EdcReceiveEvent>() {

			@Override
			public Class<EdcReceiveEvent> getEventClass() {
				return EdcReceiveEvent.class;
			}

			@Override
			public String getName() {
				return buttonTopic.getName();
			}

			@Override
			public boolean handle(EdcReceiveEvent event) {
				sendTopic.post(new SendEvent("gatekeeper", recog.recognise()));
				return true;
			}
		});
		cloudErrorTopic.addHandler(new EventHandler<EdcErrorEvent>() {

			@Override
			public Class<EdcErrorEvent> getEventClass() {
				return EdcErrorEvent.class;
			}

			@Override
			public String getName() {
				return cloudErrorTopic.getName();
			}

			@Override
			public boolean handle(EdcErrorEvent event) {
				errorTopic.post(new ErrorEvent("gatekeeper", "Impossibile aprire la porta"));
				return true;
			}
		});
	}
}
