package it.netgrid.got.gatekeeper.faces;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import it.netgrid.got.gatekeeper.properties.FacesPropertiesConfiguration;

import org.openimaj.feature.FloatFV;
import org.openimaj.feature.FloatFVComparison;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.colour.Transforms;
import org.openimaj.image.processing.face.detection.DetectedFace;
import org.openimaj.image.processing.face.detection.HaarCascadeDetector;
import org.openimaj.image.processing.face.detection.keypoints.FKEFaceDetector;
import org.openimaj.image.processing.face.detection.keypoints.KEDetectedFace;
import org.openimaj.image.processing.face.feature.FacePatchFeature;
import org.openimaj.image.processing.face.feature.FacePatchFeature.Extractor;
import org.openimaj.image.processing.face.feature.comparison.FaceFVComparator;
import org.openimaj.image.processing.face.similarity.FaceSimilarityEngine;
import org.openimaj.video.VideoDisplay;
import org.openimaj.video.VideoDisplayListener;
import org.openimaj.video.capture.VideoCapture;
import org.openimaj.video.capture.VideoCaptureException;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class WebCamRecognizer implements Runnable, Recognizer {

	private List<WebCamWrappedImage> facequeue;
	private FacesPropertiesConfiguration config;

	@Inject
	public WebCamRecognizer(FacesPropertiesConfiguration config) {
		this.config = config;
	}

	@Override
	public void run() {
		VideoCapture vc = null;
		try {
			vc = new VideoCapture(320, 240);
		} catch (VideoCaptureException e) {
			e.printStackTrace();
		}
		facequeue = new ArrayList<WebCamWrappedImage>();

		VideoDisplay<MBFImage> vd = VideoDisplay.createVideoDisplay(vc);
		vd.addVideoListener(new VideoDisplayListener<MBFImage>() {
			public void beforeUpdate(MBFImage frame) {

				WebCamWrappedImage wi = new WebCamWrappedImage(frame);
				facequeue.add(wi);
				if (facequeue.size() > 10) {
					facequeue.remove(0);
				}
				HaarCascadeDetector det1 = new HaarCascadeDetector(40);
				List<DetectedFace> faces = det1.detectFaces(Transforms.calculateIntensity(frame));
				for (DetectedFace face : faces) {
					frame.drawShape(face.getBounds(), RGBColour.YELLOW);

				}
			}

			public void afterUpdate(VideoDisplay<MBFImage> display) {

			}
		});

	}

	private String tryRecog(MBFImage image2) {
		List<FaceMatchResult> results = new ArrayList<FaceMatchResult>();
		FImage fi = Transforms.calculateIntensityNTSC(image2);
		// first, we load two images
		URL image1url = null;
		for (Object k : config.getProperties().keySet()) {
			try {
				image1url = new URL(config.getUrl(k.toString()));
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			}

			FImage image1 = null;
			try {
				image1 = ImageUtilities.readF(image1url);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			// then we set up a face detector; will use a haar cascade detector
			// to
			// find faces, followed by a keypoint-enhanced detector to find
			// facial
			// keypoints for our feature. There are many different combinations
			// of
			// features and detectors to choose from.
			final HaarCascadeDetector detector = HaarCascadeDetector.BuiltInCascade.frontalface_alt2.load();
			final FKEFaceDetector kedetector = new FKEFaceDetector(detector);

			// now we construct a feature extractor - this one will extract
			// pixel
			// patches around prominant facial keypoints (like the corners of
			// the
			// mouth, etc) and build them into a vector.
			final Extractor extractor = new FacePatchFeature.Extractor();

			// in order to compare the features we need a comparator. In this
			// case,
			// we'll use the Euclidean distance between the vectors:
			final FaceFVComparator<FacePatchFeature, FloatFV> comparator = new FaceFVComparator<FacePatchFeature, FloatFV>(
					FloatFVComparison.EUCLIDEAN);

			// Now we can construct the FaceSimilarityEngine. It is capable of
			// running the face detector on a pair of images, extracting the
			// features and then comparing every pair of detected faces in the
			// two
			// images:
			final FaceSimilarityEngine<KEDetectedFace, FacePatchFeature, FImage> engine = new FaceSimilarityEngine<KEDetectedFace, FacePatchFeature, FImage>(
					kedetector, extractor, comparator);

			// we need to tell the engine to use our images:
			engine.setQuery(image1, "image1");

			engine.setTest(fi, "image2");
			// and then to do its work of detecting, extracting and comparing
			engine.performTest();

			// finally, for this example, we're going to display the "best"
			// matching
			// faces in the two images. The following loop goes through the map
			// of
			// each face in the first image to all the faces in the second:
			FacePatchFeature face1 = null;
			FacePatchFeature face2 = null;
			for (KEDetectedFace face : engine.detector().detectFaces(image1)) {
				face1 = engine.extractor().extractFeature(face);
			}
			for (KEDetectedFace face : engine.detector().detectFaces(fi)) {
				face2 = engine.extractor().extractFeature(face);
			}

			results.add(new FaceMatchResult(engine.comparator().compare(face1, face2), k.toString()));
		}

		FaceMatchResult best = new FaceMatchResult(100, "null");
		for (FaceMatchResult r : results) {
			if (r.getMatch() < best.getMatch()) {
				best = r;
			}
		}

		if (best.getMatch() < 30) {
			return best.getName() + " è alla porta";
		} else if (best.getMatch() < 35) {
			return "Forse " + best.getName() + " è alla porta";
		}
		return "C'è qualcuno alla porta";
	}

	@Override
	public String recognise() {
		return tryRecog(facequeue.get(facequeue.size() - 1).getFace());
	}

}
