package it.netgrid.got.gatekeeper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.inject.Guice;
import com.google.inject.Injector;

import it.netgrid.got.gatekeeper.inject.modules.ConfigurationModule;

public class Main {
	static final int POOL_SIZE=1;
	public static void main(String[] args) throws InterruptedException {
		ExecutorService executor=Executors.newFixedThreadPool(POOL_SIZE);
		Injector injector = Guice.createInjector(new ConfigurationModule());
		Gatekeeper gatekeeper = injector.getInstance(Gatekeeper.class);
		executor.execute(gatekeeper);
	}

}
