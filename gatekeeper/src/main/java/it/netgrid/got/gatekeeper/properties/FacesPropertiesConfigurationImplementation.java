package it.netgrid.got.gatekeeper.properties;

import java.util.Properties;

import it.netgrid.got.utils.PropertiesConfigurationTemplate;

public class FacesPropertiesConfigurationImplementation extends PropertiesConfigurationTemplate
		implements FacesPropertiesConfiguration {

	private final String DEFAULT_CONFIG_PROPERTIES_PATH;
	private static final String DEFAULT_CONFIG_PROPERTIES_RESOURCE = "faces.properties";
	private Properties properties;

	public FacesPropertiesConfigurationImplementation(String path) {
		DEFAULT_CONFIG_PROPERTIES_PATH=path+"faces.properties";
		properties = getProperties(null);
		
	}

	@Override
	public String getName(String url) {
		for (Object k : properties.keySet()) {
			if (properties.getProperty((String) k) == url) {
				return format(k.toString());
			}
		}
		return null;
	}

	@Override
	public String getUrl(String name) {
		if (properties.containsKey(name)) {
			return properties.getProperty(name);
		}
		return null;
	}

	@Override
	public String getDefaultConfigPropertiesPath() {
		return DEFAULT_CONFIG_PROPERTIES_PATH;
	}

	@Override
	public String getDefaultConfigPropertiesResource() {
		return DEFAULT_CONFIG_PROPERTIES_RESOURCE;
	}

	private String format(String raw) {
		return raw.replace(".", " ");
	}

	@Override
	public Properties getProperties() {
		return properties;
	}
}
