package it.netgrid.got.gatekeeper.inject.modules;

import com.google.inject.AbstractModule;

public class GatekeeperEnv extends AbstractModule {

	@Override
	protected void configure() {
		install(new ConfigurationModule());
	}

}
