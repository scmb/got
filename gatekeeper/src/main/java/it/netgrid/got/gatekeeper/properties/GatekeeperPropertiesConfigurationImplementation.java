package it.netgrid.got.gatekeeper.properties;

import java.util.Properties;

import it.netgrid.got.utils.PropertiesConfigurationTemplate;

public class GatekeeperPropertiesConfigurationImplementation extends PropertiesConfigurationTemplate
		implements GatekeeperPropertiesConfiguration {

	private static final String DEFAULT_CONFIG_PROPERTIES_PATH = System.getProperty("user.dir")+"/gatekeeper.properties";
	private static final String DEFAULT_CONFIG_PROPERTIES_RESOURCE = "gatekeeper.properties";
	private Properties properties;

	public GatekeeperPropertiesConfigurationImplementation() {
		properties = getProperties(null);
	}

	@Override
	public String getDefaultConfigPropertiesPath() {
		return DEFAULT_CONFIG_PROPERTIES_PATH;
	}

	@Override
	public String getDefaultConfigPropertiesResource() {
		return DEFAULT_CONFIG_PROPERTIES_RESOURCE;
	}

	@Override
	public String getSendTopic() {
		return properties.getProperty("gatekeeper_send");
	}

	@Override
	public String getReceiveTopic() {
		return properties.getProperty("gatekeeper_receive");
	}

	@Override
	public String getErrorTopic() {
		return properties.getProperty("gatekeeper_error");
	}

	@Override
	public String getGateTopic() {
		return properties.getProperty("gate_topic");
	}

	@Override
	public String getButtonTopic() {
		return properties.getProperty("button_topic");
	}

	@Override
	public String getCloudErrorTopic() {
		return properties.getProperty("error_topic");
	}

	@Override
	public String getVideoStreamAddress() {
		return properties.getProperty("video_source");
	}

	@Override
	public String getFrameDir() {
		return properties.getProperty("frame_directory");
	}

	@Override
	public String getFacesDir() {
		return properties.getProperty("faces_directory");
	}

	@Override
	public int getQueueSize() {
		return Integer.parseInt(properties.getProperty("queue_size"));
	}

	@Override
	public int getFrameRate() {
		return Integer.parseInt(properties.getProperty("frame_rate"));
	}

}
