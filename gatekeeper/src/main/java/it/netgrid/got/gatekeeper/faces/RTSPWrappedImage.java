package it.netgrid.got.gatekeeper.faces;

import java.util.Date;

import org.bytedeco.javacv.Frame;

public class RTSPWrappedImage {
	private Frame face;
	private Date timestamp;

	public RTSPWrappedImage(Frame face) {
		setFace(face);
	}

	public Frame getFace() {
		return face;
	}

	private void setFace(Frame face) {
		this.face = face;
		setTimestamp();
	}

	public Date getTimestamp() {
		return timestamp;
	}

	private void setTimestamp() {
		this.timestamp = new Date();
	}
}
