package it.netgrid.got.gatekeeper.faces;

public interface Recognizer extends Runnable{
	public String recognise();
}
