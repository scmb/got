package it.netgrid.got.gatekeeper.inject.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import it.netgrid.bauer.Topic;
import it.netgrid.bauer.TopicFactory;
import it.netgrid.got.gatekeeper.events.ErrorEvent;
import it.netgrid.got.gatekeeper.events.ReceiveEvent;
import it.netgrid.got.gatekeeper.events.SendEvent;
import it.netgrid.got.gatekeeper.faces.RTSPStreamRecognizer;
import it.netgrid.got.gatekeeper.faces.Recognizer;
import it.netgrid.got.gatekeeper.Gatekeeper;
import it.netgrid.got.gatekeeper.events.EdcErrorEvent;
import it.netgrid.got.gatekeeper.events.EdcReceiveEvent;
import it.netgrid.got.gatekeeper.events.EdcSendEvent;
import it.netgrid.got.gatekeeper.properties.FacesPropertiesConfiguration;
import it.netgrid.got.gatekeeper.properties.FacesPropertiesConfigurationImplementation;
import it.netgrid.got.gatekeeper.properties.GatekeeperPropertiesConfiguration;
import it.netgrid.got.gatekeeper.properties.GatekeeperPropertiesConfigurationImplementation;

public class ConfigurationModule extends AbstractModule {

	@Override
	protected void configure() {

	}

	@Provides
	public Gatekeeper buildGateKeeper(Topic<SendEvent> sendTopic, Topic<ReceiveEvent> receiveTopic,
			Topic<ErrorEvent> errorTopic, Topic<EdcSendEvent> gateTopic, Topic<EdcReceiveEvent> buttonTopic,
			Topic<EdcErrorEvent> cloudErrorTopic, Recognizer recognizer) {
		return new Gatekeeper(sendTopic, receiveTopic, errorTopic, gateTopic, buttonTopic, cloudErrorTopic, recognizer);
	}

	@Provides
	@Singleton
	public GatekeeperPropertiesConfiguration buildConfiguration() {
		return new GatekeeperPropertiesConfigurationImplementation();
	}

	@Provides
	@Singleton
	public Topic<SendEvent> buildSendTopic(GatekeeperPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getSendTopic());
	}

	@Provides
	@Singleton
	public Topic<ReceiveEvent> buildReceiveTopic(GatekeeperPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getReceiveTopic());
	}

	@Provides
	@Singleton
	public Topic<ErrorEvent> buildErrorTopic(GatekeeperPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getErrorTopic());
	}

	@Provides
	@Singleton
	public Topic<EdcSendEvent> buildGateTopic(GatekeeperPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getGateTopic());
	}

	@Provides
	@Singleton
	public Topic<EdcReceiveEvent> buildButtonTopic(GatekeeperPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getButtonTopic());
	}

	@Provides
	@Singleton
	public Topic<EdcErrorEvent> buildCloudErrorTopic(GatekeeperPropertiesConfiguration conf) {
		return TopicFactory.getTopic(conf.getCloudErrorTopic());
	}

	@Provides
	@Singleton
	public Recognizer buildRecognizer(FacesPropertiesConfiguration faces,
			GatekeeperPropertiesConfiguration gatekeeper) {
		// return new WebCamRecognizer(faces);
		return new RTSPStreamRecognizer(faces, gatekeeper);
	}

	@Provides
	@Singleton
	public FacesPropertiesConfiguration buildFacesDb(GatekeeperPropertiesConfiguration gatekeeper) {
		return new FacesPropertiesConfigurationImplementation(gatekeeper.getFacesDir());
	}

}
