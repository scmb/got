package it.netgrid.got.gatekeeper.events;

public class SendEvent {
	private String sender;
	private String content;

	public SendEvent(String sender, String content) {
		this.setSender(sender);
		this.setContent(content);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

}
