package it.netgrid.got.gatekeeper.events;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EdcSendEvent {
	private String sender;
	private Map<String, Object> content;

	public EdcSendEvent(String sender, Map<String, Object> content) {
		this.setSender(sender);
		this.setContent(content);
	}

	public Map<String, Object> getContent() {
		return content;
	}

	public void setContent(Map<String, Object> content) {
		this.content = content;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

}
