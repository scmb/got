package it.netgrid.got.gatekeeper.faces;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.openimaj.feature.FloatFV;
import org.openimaj.feature.FloatFVComparison;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.processing.face.detection.HaarCascadeDetector;
import org.openimaj.image.processing.face.detection.keypoints.FKEFaceDetector;
import org.openimaj.image.processing.face.detection.keypoints.KEDetectedFace;
import org.openimaj.image.processing.face.feature.FacePatchFeature;
import org.openimaj.image.processing.face.feature.FacePatchFeature.Extractor;
import org.openimaj.image.processing.face.feature.comparison.FaceFVComparator;
import org.openimaj.image.processing.face.similarity.FaceSimilarityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.netgrid.got.gatekeeper.properties.FacesPropertiesConfiguration;
import it.netgrid.got.gatekeeper.properties.GatekeeperPropertiesConfiguration;
import it.netgrid.got.model.ComunicationUtils;

public class RTSPStreamRecognizer implements Runnable, Recognizer {

	private static int MAX_QUEUE_SIZE;
	private static int FRAMERATE;
	private static int FRAMERATE_SLEEP;
	private static final int RESULT_MATCH = 100;
	private static final int RECOG_SURE_THRESHOLD = 30;
	private static final int RECOG_MAYBE_THRESHOLD = 40;

	private List<RTSPWrappedImage> framequeue; // setted up as list for future
												// multi thread recognization
	private FacesPropertiesConfiguration faces;
	private GatekeeperPropertiesConfiguration gatekeeper;
	private Java2DFrameConverter fc;
	
	public Logger log=LoggerFactory.getLogger("RTPS stream debug");

	public RTSPStreamRecognizer(FacesPropertiesConfiguration faces, GatekeeperPropertiesConfiguration gatekeeper) {
		this.faces = faces;
		this.gatekeeper = gatekeeper;
		MAX_QUEUE_SIZE=gatekeeper.getQueueSize();
		FRAMERATE=gatekeeper.getFrameRate();
		FRAMERATE_SLEEP=1000 / FRAMERATE;
		fc = new Java2DFrameConverter();
	}

	@Override
	public String recognise() {
		try {
			Frame frame = framequeue.get(framequeue.size() - 1).getFace();
			String imageUrl = gatekeeper.getFrameDir() + "video-frame-" + System.currentTimeMillis() + ".png";
			ImageIO.write(fc.convert(frame), "png", new File(imageUrl));
			return tryRecog(imageUrl);
		} catch (Exception e) {
			return recognise();
		}
	}

	@Override
	public void run() {
		boolean go = true;
		framequeue = new ArrayList<RTSPWrappedImage>();
		FFmpegFrameGrabber g = new FFmpegFrameGrabber(gatekeeper.getVideoStreamAddress());
		try {
			g.start();
		} catch (org.bytedeco.javacv.FrameGrabber.Exception e) {
			e.printStackTrace();
		}
		while (go) {
			try {
				Thread.sleep(FRAMERATE_SLEEP);
				Frame frame = g.grab();
				if (frame.imageChannels!=0&&frame.image!=null) { // if the frame is not corrupted
					RTSPWrappedImage wi = new RTSPWrappedImage(frame);
					framequeue.add(wi);
					if (framequeue.size() > MAX_QUEUE_SIZE) {
						framequeue.remove(0);
					}
					log.debug("[frame taken at: "+frame.timestamp+"]");
				}
			} catch (InterruptedException ex) {
				try {
					g.close();
				} catch (org.bytedeco.javacv.FrameGrabber.Exception e) {
					e.printStackTrace();
				}
				go = false;
			} catch (Exception e) {

			}
		}

	}

	private String tryRecog(String recordedFaceImageAddr) {
		try {
			List<FaceMatchResult> results = new ArrayList<FaceMatchResult>();
			FImage recordedFace = null;
			File recordedFaceFile = new File(recordedFaceImageAddr);
			recordedFace = ImageUtilities.readF(recordedFaceFile);
			recordedFaceFile.delete();
			for (Object k : faces.getProperties().keySet()) {
				File storedFaceUrl = new File(gatekeeper.getFacesDir() + faces.getUrl(k.toString()));
				FImage storedFace = null;
				storedFace = ImageUtilities.readF(storedFaceUrl);

				final HaarCascadeDetector detector = HaarCascadeDetector.BuiltInCascade.frontalface_alt2.load();
				final FKEFaceDetector kedetector = new FKEFaceDetector(detector);
				final Extractor extractor = new FacePatchFeature.Extractor();
				final FaceFVComparator<FacePatchFeature, FloatFV> comparator = new FaceFVComparator<FacePatchFeature, FloatFV>(
						FloatFVComparison.EUCLIDEAN);
				final FaceSimilarityEngine<KEDetectedFace, FacePatchFeature, FImage> engine = new FaceSimilarityEngine<KEDetectedFace, FacePatchFeature, FImage>(
						kedetector, extractor, comparator);

				engine.setQuery(storedFace, "storedFace");
				engine.setTest(recordedFace, "recordedFace");
				engine.performTest();

				FacePatchFeature storedFaceRilevation = null;
				FacePatchFeature recordedFaceRilevation = null;
				for (KEDetectedFace face : engine.detector().detectFaces(recordedFace)) {
					recordedFaceRilevation = engine.extractor().extractFeature(face);
				}
				if (recordedFaceRilevation == null) {
					return ComunicationUtils.Gatekeeper.UNKNOWN;
				}
				for (KEDetectedFace face : engine.detector().detectFaces(storedFace)) {
					storedFaceRilevation = engine.extractor().extractFeature(face);
				}
				if (storedFaceRilevation != null) {
					results.add(new FaceMatchResult(
							engine.comparator().compare(storedFaceRilevation, recordedFaceRilevation), k.toString()));
				}
			}

			FaceMatchResult best = new FaceMatchResult(RESULT_MATCH, "null");
			for (FaceMatchResult r : results) {
				if (r.getMatch() < best.getMatch()) {
					best = r;
				}
			}
			return this.getRecogResult(best);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private String getRecogResult(FaceMatchResult best) {
		if (best.getMatch() < RECOG_SURE_THRESHOLD) {
			return best.getName() + ComunicationUtils.Gatekeeper.SURE;
		} else if (best.getMatch() < RECOG_MAYBE_THRESHOLD) {
			return best.getName() + ComunicationUtils.Gatekeeper.MAYBE;
		} else {
			return ComunicationUtils.Gatekeeper.UNKNOWN;
		}
	}
}
