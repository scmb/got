package org.gatekeeper;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Rule;
import org.junit.Test;

import com.google.guiceberry.junit4.GuiceBerryRule;
import com.google.inject.Inject;

import it.netgrid.got.gatekeeper.faces.Recognizer;
import it.netgrid.got.gatekeeper.properties.GatekeeperPropertiesConfiguration;

public class VideoStreamTest {
	@Rule
	public final GuiceBerryRule guiceBerry = new GuiceBerryRule(GatekeeperTestEnv.class);

	@Inject
	Recognizer recog;

	@Inject
	GatekeeperPropertiesConfiguration gatekeeper;

	ExecutorService ex = Executors.newFixedThreadPool(1);

	public void testVideoStream() throws InterruptedException {
		ex.execute(recog);
		Thread.sleep(10000);
		System.out.println(recog.recognise());
		assertTrue(true);
	}
}
