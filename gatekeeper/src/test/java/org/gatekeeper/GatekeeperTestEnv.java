package org.gatekeeper;

import com.google.guiceberry.GuiceBerryModule;

import it.netgrid.got.gatekeeper.inject.modules.GatekeeperEnv;

public class GatekeeperTestEnv extends GatekeeperEnv {

	@Override
	protected void configure() {
		super.configure();
		install(new GuiceBerryModule());
	}
}
